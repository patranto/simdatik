-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 Agu 2016 pada 04.05
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simdatik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tblbarang`
--

CREATE TABLE `tblbarang` (
  `IDBarang` char(9) NOT NULL,
  `IDSupplier` char(6) NOT NULL,
  `NamaBarang` varchar(55) NOT NULL,
  `Jenis` varchar(50) NOT NULL,
  `Harga` int(11) NOT NULL,
  `PhotoBrg` varchar(200) NOT NULL,
  `Jml_min` int(11) NOT NULL,
  `Jml_max` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tblbarang`
--

INSERT INTO `tblbarang` (`IDBarang`, `IDSupplier`, `NamaBarang`, `Jenis`, `Harga`, `PhotoBrg`, `Jml_min`, `Jml_max`) VALUES
('001CRB001', 'CRB001', 'Mie goreng', 'Mie', 1000, '', 100, 300),
('002KNG001', 'KNG001', 'Pensil 2B', 'ATK', 1000, '', 10, 100),
('002CRB001', 'CRB001', 'Stof Maf', 'ATK', 78, '', 10, 100),
('001KNG001', 'KNG001', 'Kertas Double Folio', 'ATK', 25000, '', 50, 200),
('001JKT001', 'JKT001', 'KArtu Perdana AS', 'SIM', 3000, '', 15, 50),
('003CRB001', 'CRB001', 'Spidol Permanent Snowman', 'Spidol', 5000, '', 10, 75),
('004CRB001', 'CRB001', 'Pensil 2B', 'pensil', 10000, '', 15, 90),
('005CRB001', 'CRB001', 'jhkhas', 'jhfkj', 1000, '', 10, 50),
('001MJL001', 'MJL001', 'Sabun Lux', 'Sabun Mandi', 10000, '', 25, 150),
('006CRB001', 'CRB001', 'Tes', 'Tes', 12, '', 5, 15),
('007CRB001', 'CRB001', 'kjlkj', 'kjk', 1, '', 1, 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tblindi`
--

CREATE TABLE `tblindi` (
  `IDIndi` char(8) NOT NULL,
  `NamaIndi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tblindi`
--

INSERT INTO `tblindi` (`IDIndi`, `NamaIndi`) VALUES
('LP', 'Luas Panen'),
('LT', 'Luas Tanam'),
('PR', 'Produksi'),
('PT', 'Produktivitas'),
('TB', 'Tanaman Belum Menghasilkan'),
('TH', 'Tanaman Menghasilkan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tblisinya`
--

CREATE TABLE `tblisinya` (
  `Subsektor` varchar(200) NOT NULL,
  `Komoditi` varchar(200) NOT NULL,
  `Indikator` varchar(200) NOT NULL,
  `Kecamatan` varchar(200) NOT NULL,
  `Tahun` int(11) NOT NULL,
  `jan` int(11) NOT NULL,
  `peb` int(11) NOT NULL,
  `mar` int(11) NOT NULL,
  `apr` int(11) NOT NULL,
  `mei` int(11) NOT NULL,
  `jun` int(11) NOT NULL,
  `jul` int(11) NOT NULL,
  `ags` int(11) NOT NULL,
  `sep` int(11) NOT NULL,
  `okt` int(11) NOT NULL,
  `nov` int(11) NOT NULL,
  `des` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tblisinya`
--

INSERT INTO `tblisinya` (`Subsektor`, `Komoditi`, `Indikator`, `Kecamatan`, `Tahun`, `jan`, `peb`, `mar`, `apr`, `mei`, `jun`, `jul`, `ags`, `sep`, `okt`, `nov`, `des`) VALUES
('Hortikultura', 'Alpukat', 'Luas Panen', 'Majalengka', 2014, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
('Hortikultura', 'Anggrek', 'Luas Panen', 'Majalengka', 2016, 0, 50, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tblkomoditi`
--

CREATE TABLE `tblkomoditi` (
  `IDKomoditi` char(8) NOT NULL,
  `IDSektor` varchar(50) NOT NULL,
  `NamaKomoditi` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tblkomoditi`
--

INSERT INTO `tblkomoditi` (`IDKomoditi`, `IDSektor`, `NamaKomoditi`) VALUES
('ALP', 'Hortikultura', 'Alpukat'),
('ANG', 'Hortikultura', 'Anggrek'),
('JG', 'Tanaman_Pangan', 'Jagung'),
('KCJ', 'Tanaman_Pangan', 'Kacang Hijau'),
('MS', 'Perikanan', 'Ikan Mas'),
('NL', 'Perikanan', 'Nila');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tblsektor`
--

CREATE TABLE `tblsektor` (
  `IDSektor` char(6) NOT NULL,
  `NamaSektor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tblsektor`
--

INSERT INTO `tblsektor` (`IDSektor`, `NamaSektor`) VALUES
('HT', 'Hortikultura'),
('IK', 'Perikanan'),
('TP', 'Tanaman_Pangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tblsupplier`
--

CREATE TABLE `tblsupplier` (
  `IDSupplier` char(6) NOT NULL,
  `NamaSupplier` varchar(35) NOT NULL,
  `AlamatSupplier` varchar(100) NOT NULL,
  `Telepon` varchar(13) NOT NULL,
  `web` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tblsupplier`
--

INSERT INTO `tblsupplier` (`IDSupplier`, `NamaSupplier`, `AlamatSupplier`, `Telepon`, `web`) VALUES
('32', 'Majalengka', 'Jl.Gerakan Koperasi No.1 Majalengka 45411', '0233', '-'),
('20', 'Sumberjaya', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbltransaksi`
--

CREATE TABLE `tbltransaksi` (
  `IDTransaksi` int(11) NOT NULL,
  `IDBarang` char(9) NOT NULL,
  `TglTransaksi` date NOT NULL,
  `Keterangan` varchar(50) NOT NULL,
  `Jumlah` int(11) NOT NULL,
  `Status` enum('M','K') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbltransaksi`
--

INSERT INTO `tbltransaksi` (`IDTransaksi`, `IDBarang`, `TglTransaksi`, `Keterangan`, `Jumlah`, `Status`) VALUES
(1, '002KNG001', '2012-02-29', '-', 20, 'M'),
(2, '004CRB001', '2012-02-29', '-', 30, 'M'),
(3, '002KNG001', '2012-03-02', '-', 10, 'K'),
(4, '002KNG001', '2016-07-13', '-', 10, 'M'),
(5, '002KNG001', '2016-07-13', '-', 6, 'K'),
(6, '002KNG001', '2016-07-13', '-', -2, 'K');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbluser`
--

CREATE TABLE `tbluser` (
  `IDUser` varchar(15) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `NamaUser` varchar(35) NOT NULL,
  `Level` enum('beli','sales','manajer') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbluser`
--

INSERT INTO `tbluser` (`IDUser`, `Password`, `NamaUser`, `Level`) VALUES
('user', 'ee11cbb19052e40b07aac0ca060c23ee', 'Operator', 'beli'),
('jual', '983bde2c91fd60681662c6debda4fdd3', 'Nurul Enda', 'sales'),
('admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'manajer');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_barang_supplier`
--
CREATE TABLE `v_barang_supplier` (
`IDBarang` char(9)
,`IDSupplier` char(6)
,`NamaBarang` varchar(55)
,`Jenis` varchar(50)
,`Harga` int(11)
,`PhotoBrg` varchar(200)
,`NamaSupplier` varchar(35)
,`AlamatSupplier` varchar(100)
,`Telepon` varchar(13)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_laporan_pembelian`
--
CREATE TABLE `v_laporan_pembelian` (
`IDTransaksi` int(11)
,`IDBarang` char(9)
,`TglTransaksi` date
,`Keterangan` varchar(50)
,`Jumlah` int(11)
,`NamaBarang` varchar(55)
,`Jenis` varchar(50)
,`Harga` int(11)
,`PhotoBrg` varchar(200)
,`NamaSupplier` varchar(35)
,`AlamatSupplier` varchar(100)
,`Telepon` varchar(13)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_laporan_penjualan`
--
CREATE TABLE `v_laporan_penjualan` (
`IDTransaksi` int(11)
,`IDBarang` char(9)
,`TglTransaksi` date
,`Keterangan` varchar(50)
,`Jumlah` int(11)
,`NamaBarang` varchar(55)
,`Jenis` varchar(50)
,`Harga` int(11)
,`PhotoBrg` varchar(200)
,`NamaSupplier` varchar(35)
,`AlamatSupplier` varchar(100)
,`Telepon` varchar(13)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_barang_supplier`
--
DROP TABLE IF EXISTS `v_barang_supplier`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_barang_supplier`  AS  select `tblbarang`.`IDBarang` AS `IDBarang`,`tblbarang`.`IDSupplier` AS `IDSupplier`,`tblbarang`.`NamaBarang` AS `NamaBarang`,`tblbarang`.`Jenis` AS `Jenis`,`tblbarang`.`Harga` AS `Harga`,`tblbarang`.`PhotoBrg` AS `PhotoBrg`,`tblsupplier`.`NamaSupplier` AS `NamaSupplier`,`tblsupplier`.`AlamatSupplier` AS `AlamatSupplier`,`tblsupplier`.`Telepon` AS `Telepon` from (`tblbarang` left join `tblsupplier` on((`tblsupplier`.`IDSupplier` = `tblbarang`.`IDSupplier`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_laporan_pembelian`
--
DROP TABLE IF EXISTS `v_laporan_pembelian`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_laporan_pembelian`  AS  select `tbltransaksi`.`IDTransaksi` AS `IDTransaksi`,`tbltransaksi`.`IDBarang` AS `IDBarang`,`tbltransaksi`.`TglTransaksi` AS `TglTransaksi`,`tbltransaksi`.`Keterangan` AS `Keterangan`,`tbltransaksi`.`Jumlah` AS `Jumlah`,`tblbarang`.`NamaBarang` AS `NamaBarang`,`tblbarang`.`Jenis` AS `Jenis`,`tblbarang`.`Harga` AS `Harga`,`tblbarang`.`PhotoBrg` AS `PhotoBrg`,`tblsupplier`.`NamaSupplier` AS `NamaSupplier`,`tblsupplier`.`AlamatSupplier` AS `AlamatSupplier`,`tblsupplier`.`Telepon` AS `Telepon` from ((`tbltransaksi` left join `tblbarang` on((`tblbarang`.`IDBarang` = `tbltransaksi`.`IDBarang`))) left join `tblsupplier` on((`tblsupplier`.`IDSupplier` = `tblbarang`.`IDSupplier`))) where (`tbltransaksi`.`Status` = 'M') ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_laporan_penjualan`
--
DROP TABLE IF EXISTS `v_laporan_penjualan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_laporan_penjualan`  AS  select `tbltransaksi`.`IDTransaksi` AS `IDTransaksi`,`tbltransaksi`.`IDBarang` AS `IDBarang`,`tbltransaksi`.`TglTransaksi` AS `TglTransaksi`,`tbltransaksi`.`Keterangan` AS `Keterangan`,`tbltransaksi`.`Jumlah` AS `Jumlah`,`tblbarang`.`NamaBarang` AS `NamaBarang`,`tblbarang`.`Jenis` AS `Jenis`,`tblbarang`.`Harga` AS `Harga`,`tblbarang`.`PhotoBrg` AS `PhotoBrg`,`tblsupplier`.`NamaSupplier` AS `NamaSupplier`,`tblsupplier`.`AlamatSupplier` AS `AlamatSupplier`,`tblsupplier`.`Telepon` AS `Telepon` from ((`tbltransaksi` left join `tblbarang` on((`tblbarang`.`IDBarang` = `tbltransaksi`.`IDBarang`))) left join `tblsupplier` on((`tblsupplier`.`IDSupplier` = `tblbarang`.`IDSupplier`))) where (`tbltransaksi`.`Status` = 'K') ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblbarang`
--
ALTER TABLE `tblbarang`
  ADD PRIMARY KEY (`IDBarang`);

--
-- Indexes for table `tblindi`
--
ALTER TABLE `tblindi`
  ADD PRIMARY KEY (`IDIndi`);

--
-- Indexes for table `tblkomoditi`
--
ALTER TABLE `tblkomoditi`
  ADD PRIMARY KEY (`IDKomoditi`);

--
-- Indexes for table `tblsektor`
--
ALTER TABLE `tblsektor`
  ADD PRIMARY KEY (`IDSektor`);

--
-- Indexes for table `tblsupplier`
--
ALTER TABLE `tblsupplier`
  ADD PRIMARY KEY (`IDSupplier`);

--
-- Indexes for table `tbltransaksi`
--
ALTER TABLE `tbltransaksi`
  ADD PRIMARY KEY (`IDTransaksi`);

--
-- Indexes for table `tbluser`
--
ALTER TABLE `tbluser`
  ADD PRIMARY KEY (`IDUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbltransaksi`
--
ALTER TABLE `tbltransaksi`
  MODIFY `IDTransaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
